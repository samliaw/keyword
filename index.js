"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var pfs = require("fs/promises");
var path = require("path");
var config = {
    keyword: new RegExp('TODO'),
    filteringFiles: new RegExp('/*')
};
function bootstrap() {
    return __awaiter(this, void 0, void 0, function () {
        var outputFiles, args, parentDirectory, existDirectory, error_1, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    outputFiles = [];
                    args = process.argv.slice(2);
                    _setConfig(args[0] || 'TODO', args[1] || '/*');
                    parentDirectory = args[2] || process.cwd();
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, pfs.stat(parentDirectory)];
                case 2:
                    existDirectory = _a.sent();
                    return [3 /*break*/, 4];
                case 3:
                    error_1 = _a.sent();
                    console.log(parentDirectory, "doesn't seem to be a valid directory. Please check and try again");
                    return [2 /*return*/];
                case 4: return [4 /*yield*/, getSubDirectoriesFiles(parentDirectory, outputFiles)];
                case 5:
                    result = _a.sent();
                    return [4 /*yield*/, getSubDirectoriesFiles(parentDirectory, outputFiles)];
                case 6:
                    _a.sent();
                    console.log(outputFiles.toString().replace(/,/g, '\n'));
                    return [2 /*return*/];
            }
        });
    });
}
function getSubDirectoriesFiles(directory, outputFiles) {
    return __awaiter(this, void 0, void 0, function () {
        var files, directories, _i, files_1, file, filePath, fileDirectory, foundKeyword, _a, directories_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, pfs.readdir(directory)];
                case 1:
                    files = _b.sent();
                    directories = [];
                    _i = 0, files_1 = files;
                    _b.label = 2;
                case 2:
                    if (!(_i < files_1.length)) return [3 /*break*/, 7];
                    file = files_1[_i];
                    filePath = path.join(directory, file);
                    return [4 /*yield*/, pfs.stat(filePath)];
                case 3:
                    fileDirectory = _b.sent();
                    if (!fileDirectory.isDirectory()) return [3 /*break*/, 4];
                    directories.push(filePath);
                    return [3 /*break*/, 6];
                case 4: return [4 /*yield*/, readFileContentAndSearchForKeyword(filePath)];
                case 5:
                    foundKeyword = _b.sent();
                    // console.log('reading file ...', foundKeyword, '-----', filePath);
                    if (foundKeyword) {
                        outputFiles.push(filePath);
                    }
                    _b.label = 6;
                case 6:
                    _i++;
                    return [3 /*break*/, 2];
                case 7:
                    _a = 0, directories_1 = directories;
                    _b.label = 8;
                case 8:
                    if (!(_a < directories_1.length)) return [3 /*break*/, 11];
                    directory = directories_1[_a];
                    return [4 /*yield*/, getSubDirectoriesFiles(directory, outputFiles)];
                case 9:
                    _b.sent();
                    _b.label = 10;
                case 10:
                    _a++;
                    return [3 /*break*/, 8];
                case 11: return [2 /*return*/];
            }
        });
    });
}
function readFileContentAndSearchForKeyword(filePath) {
    return __awaiter(this, void 0, void 0, function () {
        var content;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!config.filteringFiles.test(filePath)) {
                        return [2 /*return*/, false];
                    }
                    return [4 /*yield*/, pfs.readFile(filePath, 'utf8')];
                case 1:
                    content = _a.sent();
                    return [2 /*return*/, config.keyword.test(content) ? true : false];
            }
        });
    });
}
function _setConfig(keyword, filteringFiles) {
    if (keyword) {
        config.keyword = new RegExp(keyword);
    }
    if (filteringFiles) {
        config.filteringFiles = new RegExp(filteringFiles);
    }
}
bootstrap();
