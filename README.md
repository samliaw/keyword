# Program

The program searches for specific keyword and show the files containing the keywork. 

# Install Program
nodejs must be installed. Refer to here for info. https://nodejs.org/en/download/

Clone the git project using the command git clone https://gitlab.com/samliaw/keyword.
After it is done, cd to the keyword directory. There are 6 files as follow
```
index.ts
index.js
package.json
package-lock.json
.gitignore
README.md
```
run the command "npm i" to install all dependencies. 

# How to use the program. 
1. You can simply run the program as -> node index.js.
2. You can pass in the 3 parameters as in this format -> node index.js "TODO" "/*" "<directory>". Default to directory where the node is run. 
The 1st parameter defines the keyword. It is case sensitive. TODO and todo are different.
The 2nd parameter defines what files to include to search for keyword.
The 3rd parameter defines the directory where the search begins. It defaults to current directory where node is run.
You can supply parameter in the following 
- 1st, 2nd, 3rd, or
- 1st, 2nd or
- 1st, or
- none

3. Some examples of the parameters are as follow
- node index.js "TODO|todo". It will search keyword "todo" and "TODO" for all files in this directory and all sub-directories. 
- node index.js "TODO" "/*.json|/*.ts". It will search keyword "TODO" for all files ending with ".json" or ".ts" in this directory and all sub-directories. 
- node index.js "TODO" "/*.json|/*.ts" "/home/sam/program/". It will search keyword "TODO" for all files ending with ".json" or ".ts" in this directory "/home/sam/program/" and all its sub-directories. 

4. In case if an invalid directory is provided, it will highlight it and exits. 

