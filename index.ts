import pfs = require('fs/promises');
import path = require('path');

const config: Record<string, RegExp> = {
    keyword: new RegExp('TODO'),
    filteringFiles: new RegExp('/*')
}

async function bootstrap() {

    let outputFiles: Array<string> = [];
    const args = process.argv.slice(2);
    _setConfig(args[0] || 'TODO', args[1] || '/*');
    const parentDirectory = args[2] || process.cwd();

    try {
        const existDirectory = await pfs.stat(parentDirectory);
    } catch (error) {
        console.log(parentDirectory, "doesn't seem to be a valid directory. Please check and try again");
        return;
    }
  
    const result = await getSubDirectoriesFiles(parentDirectory, outputFiles);
    await getSubDirectoriesFiles(parentDirectory, outputFiles);

    console.log(outputFiles.toString().replace(/,/g, '\n'));
 
}

async function getSubDirectoriesFiles(directory: string, outputFiles: Array<string>) {

    const files = await pfs.readdir(directory);
    let directories: Array<string> = [];

    // let filesWithKeywords: Array<string> = [];

    for (const file of files) {
        const filePath = path.join(directory, file);
        const fileDirectory = await pfs.stat(filePath);
        if (fileDirectory.isDirectory()) {
            directories.push(filePath)
        } else {
            const foundKeyword = await readFileContentAndSearchForKeyword(filePath);
            // console.log('reading file ...', foundKeyword, '-----', filePath);
            if (foundKeyword) {
                outputFiles.push(filePath);
            }
        }
    }

    for (directory of directories) {
        await getSubDirectoriesFiles(directory, outputFiles)
    }

}


async function readFileContentAndSearchForKeyword(filePath: string): Promise<boolean> {

    if (!config.filteringFiles.test(filePath)) {
        return false;
    }

    const content = await pfs.readFile(filePath, 'utf8');
    return config.keyword.test(content) ? true : false;

}

function _setConfig(keyword, filteringFiles) {

    if (keyword) {
        config.keyword = new RegExp(keyword);
    }

    if (filteringFiles) {
        config.filteringFiles = new RegExp(filteringFiles);
    }

}

bootstrap();